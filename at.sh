#!/bin/bash

while true; do
    # Exibir menu
    echo -e ================= "Menu:"======================================
    echo "a -> Executar ping para um host (ip ou site)"
    echo "b -> Listar os usuários atualmente logados na máquina"
    echo "c -> Exibir o uso de memória e de disco da máquina"
    echo "d -> Sair"
    echo -e ==============================================================
    # Ler a opção do usuário
    read -p "Escolha uma opção: " opcao

    # Processar a opção escolhida
    case $opcao in
        a)
            read -p "Digite o host (ip ou site) para executar o ping: " host
            ping -c 2 "$host"
            ;;
        b)
            who
            ;;
        c)
            echo "Uso de memória:"
            free -h
            echo "Uso de disco:"
            df -h
            ;;
        d)
            echo "Saindo do script. Até mais!"
            exit 0
            ;;
        *)
            echo "Opção inválida. Tente novamente."
            ;;
    esac
done
